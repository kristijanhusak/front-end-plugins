<?php
include('diffClass.php');
$diff = new Diff();

echo $diff->header();

$val = '';

if ($_POST['file'])
{
	$val = $diff->readDiff($_POST['file']);
    echo $diff->showDownloadButton($val);
    echo $val;
    unset($_POST);
}
echo $diff->footer();
?>


