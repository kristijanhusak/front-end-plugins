<?php

class Diff {

	public $filename = 'tempdiff.html';


	public function readDiff($file) 
	{
		$file = explode("\n", $file);
		$text = '';
		foreach ($file as $f)
		{
			$text .= $this->matchText($f)."\n";
		}
		return $text;
	}

	private function matchText($row)
	{
        if (!preg_match('/^(\+|\-).*/', $row))
        {
            return preg_replace('/(^.*)/', '<xmp>$1</xmp>', $row);
        }

        if (preg_match('/^\+[^\+\+].*/', $row))
        {
            return preg_replace('/(^\+[^\+\+].*)/', '<span class="green-diff-code"><xmp>$1</xmp></span>', $row);
        }

        if (preg_match('/^\-[^\-\-].*/', $row))
        {
            return preg_replace('/(^\-[^\-\-].*)/', '<span class="red-diff-code"><xmp>$1</xmp></span>', $row);
        }

        if (preg_match('/^\+\+\+.*/', $row))
        {
            return preg_replace('/(^\+\+\+.*)/', '<span class="new-diff-file"><xmp>$1</xmp></span>', $row);
        }

        if (preg_match('/^\-\-\-.*/', $row))
        {
            return preg_replace('/(^\-\-\-.*)/', '<span class="old-diff-file"><xmp>$1</xmp></span>', $row);
        }
	}

	public function header () 
	{
	 return '<!doctype html>
	<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <title>Diff highlighter</title>
	</head>
	<style>
	  body {
	    background: #fff;
	  }

	  .red-diff-code, .green-diff-code, .old-diff-file, .new-diff-file {
	    display: inline-block;
	    height: auto;
	    width: 100%;
	    line-height: 1em;
	  }
	  
	  .red-diff-code {
	    background: #FACACA;
	  }

	  .green-diff-code {
	    background: #B5FFB5;
	  }

	  .old-diff-file {
	  	font-weight: bold;
	    background: #8CD0F5;
	  }

	  .new-diff-file {
	  	font-weight: bold;
	    background: #47B8F5;
	  }

	  .save-html-form {
	    display: block;
	    text-align: center;
	    margin: 1em 0;
	  }

	  xmp {
	    margin: 0;
	    padding: 0;
	    height: auto;
	    line-height: 1.6em;
	  }
	</style>
	<body>';  
	}

	public function footer() 
	{
	    return '</body>
				</html>';
	}

	public function showDownloadButton ($val) 
	{
	    return '<form action="save.php" method="POST" class="save-html-form">
	    <textarea name="htmlval" style="display: none;">'.htmlentities($val).'</textarea>
	    <input type="submit" name="save_html" value="Save as html" /> 
	     &nbsp; &nbsp; &nbsp; <a href="./">Paste another diff</a>
	</form>'; 
	}

	public function writeToTemp($val) 
	{

		$handle = fopen($this->filename, 'w');
		fwrite($handle, $this->header());
		$v = explode("\n", $val);
		foreach ($v as $value)
		{
			fwrite($handle, $value);
		}
		fwrite($handle, $this->footer());
	    fclose($handle);
	    $this->setHeaders();
	}

	public function setHeaders() 
	{
	if(ini_get('zlib.output_compression')) 
	{ 
		ini_set('zlib.output_compression', 'Off');	
	}

	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($this->filename)).' GMT');
	header('Cache-Control: private',false);
	header('Content-Type: application/force-download');
	header('Content-Disposition: attachment; filename="'.basename($this->filename).'"');
	header('Content-Transfer-Encoding: binary');
	header('Connection: close');
	readfile($this->filename);
	exit();
	}
}