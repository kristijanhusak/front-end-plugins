<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Diff highlighter</title>
</head>
<script>
function validate(e) {
  var val = document.forms['form1']['file'].value, el = document.getElementById('file');
  if (!val) {
    el.style.border = "2px solid red";
    el.focus();
    return false;
  } else {
    el.style.border = "1px solid";
  }
}
</script>
<style>
  body {
    background: #fff;
    font-family: Arial, Verdana, sans-serif;
  }

  h2, p {
    text-align: center;
  }

  .red-diff-code, .green-diff-code {
    display: inline-block;
    height: auto;
    width: 100%;
    line-height: 1em;
  }
  
  .red-diff-code {
    background: #FACACA;
  }

  .green-diff-code {
    background: #B5FFB5;
  }

  xmp {
    margin: 0;
    padding: 0;
    height: auto;
    line-height: 1.5em;
  }

  input[type="submit"] {
    font-size: 1.1em;
    display: block;
    margin: 1em auto;
    width: 25%;
  }

  form {
    width: 90%;
    display: block;
    margin: 1em auto;
  }

  textarea {
    width: 100%;
    display: block;
    height: 300px;
  }
</style>
<body>
  <h2>Diff highlighter</h2>
  <p>Paste the diff code into the field.</p>
<form action="diff-downloader.php" method="POST" onsubmit="return validate();" name="form1" enctype="multipart/form-data">
	<textarea name="file" id="file"></textarea>
	<input type="submit">
</form>
</body>
</html>


