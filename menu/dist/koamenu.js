/*
 * Koamenu
 *
 *
 * Copyright (c) 2012 Kristijan Husak & Aleksandar Gosevski
 * Updated by: Ivan Tatic ( ivan@simplified.me )
 * Licensed under the MIT, GPL licenses.
 */

(function($) {
    "use strict";
    var namespace = 'slidemenu';

    function SlideMenu(element, options) {
        if (!element) return null;

        var o = this.options = $.extend({}, $.fn.slideMenu.defaults, options);

        this.isOpen = false;
        this.$element = $(element);
        this.$menu = $(o.menu);

        this.setup();

        this._appendOverlay();
        this._bindEvents();

    }

    SlideMenu.prototype = {
        setup: function () {
            var o = this.options;
            this.contentDirection = (o.direction === 'left') ? o.menuWidth : '-' + o.menuWidth;
            this.$content = $(o.mainContent).css({ position: 'relative', minHeight : window.innerHeight, left: 0});
            this.$menu.css(o.direction, '-' + o.menuWidth).css('width', o.menuWidth);
        },

        open: function (e) {
            if (e) e.preventDefault();
            this._openMenu();
        },

        close: function (e) {
            if (e) e.preventDefault();
            this._closeMenu();
        },

        resize: function () {
            this.$content.css('min-height', window.innerHeight);
        },

        _openMenu: function () {
            if (this.isOpen) return;
            var o = this.options,
                contentCss = {
                'min-height': this.$menu.height(),
                left: this.contentDirection
            }, menuCss = {};

            menuCss[o.direction] = '0%';

            $.extend(contentCss, this._transition('all', this._getSpeedInSeconds()));
            $.extend(menuCss, this._transition('all', this._getSpeedInSeconds()));

            this.$content.css(contentCss);
            this.$menu.css(menuCss);
            this._handleOverlay('show');
            this.isOpen = true;
            o.callbackOpened.call(this);
        },

        _closeMenu: function () {
            if (!this.isOpen) return;

            var o = this.options,
                contentCss = {
                    'min-height': window.innerHeight,
                    left: 0
                }, menuCss = {};

            menuCss[o.direction] = '-' + o.menuWidth;

            $.extend(contentCss, this._transition('all', this._getSpeedInSeconds()));
            $.extend(menuCss, this._transition('all', this._getSpeedInSeconds()));

            this.$content.css(contentCss);
            this.$menu.css(menuCss);
            this._handleOverlay('hide');
            this.isOpen = false;
            o.callbackClosed.call(this);
        },

        _handleOverlay: function(method) {
            var self = this;
            setTimeout(function() {
                self.$overlay[method]();
            }, this.options.speed);
        },

        _getSpeedInSeconds: function () {
            return this.options.speed/1000 + 's';
        },

        _appendOverlay: function () {
            this.$overlay = $('<div>', { 'class': 'menu-overlay' });
            this.$content.append( this.$overlay.css( this.options.overlay ).hide() );
        },

        _bindEvents: function () {
            this.$element.on('click.' + namespace, $.proxy(this.open, this));
            this.$overlay.on('click.' + namespace, $.proxy(this.close, this));

            if ('orientationchange' in window) {
                window.addEventListener('orientationchange', $.proxy(this.resize, this));
            } else {
                window.addEventListener('resize', $.proxy(this.resize, this));
            }
        },

        _transition: function (prop, speed) {
            var propertyValue = [prop, speed].join(' ');
            return {
                WebkitTransition : propertyValue,
                MozTransition    : propertyValue,
                MsTransition     : propertyValue,
                OTransition      : propertyValue,
                transition       : propertyValue
            };
        }
    };

    $.fn.slideMenu = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new SlideMenu(this, options)));
            }

            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                data[option]();
            }
        });
    };

    $.fn.slideMenu.Constructor = SlideMenu;
    $.fn.slideMenu.defaults = {
        direction : 'left',
        menu : '.menu',
        mainContent : '.main-content',
        speed : 300,
        menuWidth : '50%',
        callbackOpened: function () {},
        callbackClosed: function () {},
        overlay: {
            left: 0,
            right: 0,
            bottom: 0,
            top: 0,
            position: 'absolute',
            background: 'rgba(0,0,0,.4)',
            userSelect: 'none',
            zIndex: '99'
        }
    };

}(jQuery));
