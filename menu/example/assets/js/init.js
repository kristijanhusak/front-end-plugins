$(function () {
	$('.btn-left').slideMenu({
		menuWidth: '70%',
		'menu': '.menu-left',
        callbackOpened: function () {
            console.log(this);
        }
	});

	$('.btn-right').slideMenu({
		menuWidth: '90%',
		direction: 'right',
		menu: '.menu-right'
	});

});
