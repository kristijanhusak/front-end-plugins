/*
 * Popup
 *
 * Copyright (c) 2014 Kristijan Husak
 * Licensed under the MIT, GPL licenses.
 *
 * Possible events for subscribing to (example: $('.popup'), on(eventName, function (event, context) {});
 * 'popup.loaded'
 * 'popup.open'
 * 'popup.opened'
 * 'popup.close'
 * 'popup.closed'
 */

(function($) {
    "use strict";
    var namespace = 'popup';

    function Popup(popup, options) {
        // Return if no element
        if (!popup) return null;

        // Merge custom options with defaults
        this.options = $.extend({}, $.fn.popup.defaults, options);

        // Set the flag for checking if its opened to false by default
        this.isOpened = false;

        // Cache the popup element for later use
        this.$popup = $(popup);

        // Set all elements and bind the events
        this.setup();

    }

    Popup.prototype = {

        /**
         * Set up all the variables, events, and trigger load event
         */
        setup: function() {
            var o = this.options;
            // Wrap the popup if not already wrapped
            if (!this.$popup.parent(o.wrapper).length) {
                this.$popup.wrap('<div class="' + o.wrapper.slice(1) + '"></div>');
            }

            // Get the parrent wrapper
            this.$wrapper = this.$popup.parent(o.wrapper);

            // Get the open button from options, if not jquery object, instantiate it as so
            this.$openBtn = (o.openBtn instanceof jQuery) ? o.openBtn : $(o.openBtn);

            // Set close btn to data attribute if exists
            if (!o.closeBtn) {
                this.$closeBtn = this.$wrapper.find('[data-close-popup]');
            } else {
                this.$closeBtn = $(o.closeBtn);
            }

            // Callback functions
            this.onLoaded = o.onLoaded || function () {};
            this.onOpen = o.onOpen || function () {};
            this.onOpened = o.onOpened || function () {};
            this.onClose = o.onClose || function () {};
            this.onClosed = o.onClosed || function () {};
            this.ajaxOpen = o.ajaxOpen || false;
            this.ajaxClose = o.ajaxClose || false;

            this.unbindEvents();
            this.bindEvents();

            // Trigger ons when everything loads
            this._triggerCustomEvent('loaded');
        },

        /**
         * Destroy the popup
         */
        destroy: function () {
            this.close();
            this.unbindEvents();
            this.$popup.off('.' + namespace);
            this.$popup.removeData(namespace);
        },

        /**
         * Opens the popup. If ajax function exists, executes it and calls opening after it finishes
         */
        open: function(e) {
            if (e) e.preventDefault();

            this._openPopup();
        },

        /**
         * Close the popup. If ajax function exists, executes it and calls closing after it finishes
         */
        close: function(e) {
            if (e) e.preventDefault();

            this._closePopup( e && $(e.target) );
        },

        /**
         * Reset the popup position
         * Triggered when the window is resized or orientation is changed
         */
        setPosition : function () {
            this._setPosition();
        },

        /**
         * Unbind all events
         */
        unbindEvents: function () {
            this._handleEvents('off');
        },

        /**
         * Bind all events
         */
        bindEvents: function () {
            this._handleEvents();
        },


        /**
         * Does popup window setup when "position" option is set to "element"
         */
        _positionElement: function () {
            var position = this.$openBtn.offset();

            this.$popup.css({
                top : position.top,
                left : position.left,
            });
        },

        /**
         * Position the popup to the center of the screen
         */
        _positionCenter: function () {
            var topVal = $(window).innerHeight()/2 - this.$popup.innerHeight()/2;

            this.$popup.css({
                'margin-top' : ( (topVal < 10) ? this.options.minTopValue : topVal )
            });
        },

        /**
         * Handles calling proper positioning method
         */
        _setPosition: function () {
            var o = this.options;

            if (!this.isOpened) return;


            if (o.position === 'element') {
                this._positionElement();
            } else if (o.position === 'center') {
                this._positionCenter();
            }

            this.$popup.css('visibility', 'visible')
                        .animate({opacity: 1}, 200);
        },

        /**
         * Opens the popup
         * @param {Boolean} onload If true, triggers timeout to be hidden if provided
         */
        _openPopup: function(onload) {
            var self = this;

            // Prevent popup opening if the onOpen function returns false
            if (this._triggerCustomEvent('open', true) === false) {
                return false;
            }

            this.$wrapper.addClass('loading')
                .fadeIn('fast', function () {
                    // IF the ajax function on open is provided, show popup after it finishes
                    if (self.ajaxOpen !== false) {
                        self.ajaxOpen(self)
                            .done($.proxy(self._afterOpen, self, onload));
                    } else {
                        self._afterOpen(onload);
                    }
                });
        },

        /**
         * Helper function for triggering custom events
         * @param {String} eventName name of the event
         * @param {Boolean} preventExecution should function return false and prevent "on" events from triggering if function returns false
         */
        _triggerCustomEvent: function (eventName, preventExecution) {
            var prevent = preventExecution || false,
                funcEventName = eventName.charAt(0).toUpperCase() + eventName.slice(1),
                func = this['on' + funcEventName];

            if (func && func(this) === false && prevent) {
                return false;
            } else {
                this.$popup.trigger(eventName + '.' + namespace, [this]);
            }
        },

        /**
         * Triggered after wrapper is shown and popup needs to be presented
         * @param {Boolean} onload If true, triggers timeout to be hidden if provided
         */
        _afterOpen: function (onload) {
            // Prevent executing if the wrapper is not visible
            if (!this.$wrapper.is(':visible')) return;

            var o = this.options;

            this.isOpened = true;

            this._setPosition();
            // Trigger ons after everything opened
            this._triggerCustomEvent('opened');

            // Add opened class to button
            this.$openBtn.addClass(o.popupOpenedButtonClass);

            // Auto hide popup if timeout > 0 and popup is showed on load or auto hide option is set
            if (onload || o.autoHideEveryTime) {
                this._hideTimeout();
            }

            // Add class to disable close on overlay click if set up in options
            if (!o.closeOnOverlayClick) {
                this.$wrapper.addClass('prevent-hide');
            } else {
                this.$wrapper.removeClass('prevent-hide');
            }

            this._setResizeEvent();

            // Remove loading class from overlay because everything is set up
            this.$wrapper.removeClass('loading');
        },

        /**
         * Closes the popup and hides overlay
         * @param {Object} $elem jquery elem that is used to check if the popup can be closed when clicking on it
         */
        _closePopup: function ($elem) {
            var self = this;
            // Prevent popup closing if the onClose function returns false
            if (this._triggerCustomEvent('close', true) === false) {
                return false;
            }

            // If element that is clicked for closing has an option to disable
            if (!this._isClosable($elem)) return false;

            // Trigger before closing event
            this._triggerCustomEvent('closing');

            // Clearing timer for auto hide timeout
            this._clearTimeout();

            this.$wrapper.fadeOut('fast', function () {
                // If ajax function on close is provided, wait for it before closing
                if (self.ajaxClose !== false) {
                    self.ajaxClose(self)
                        .done($.proxy(self._afterClose, self));
                } else {
                    self._afterClose(onload);
                }
            });
        },

        /**
         * Triggered after wrapper is hidden
         */
        _afterClose: function () {
            this.isOpened = false;

            this.$popup.css({
                opacity: 0,
                visibility: 'hidden'
            });

            // Function called after popup is closed
            this.onClosed(this);
            this._triggerCustomEvent('closed');

            // Remove classes that are not needed any more
            this.$openBtn.removeClass(this.options.popupOpenedButtonClass);

            this._setResizeEvent('off');

            this.$wrapper.removeClass('loading');
        },

        /**
         * Calls the close method after the given timeout from options
         */
        _hideTimeout: function () {
            var self = this, o = this.options;
            if (o.hideTimeout > 0 && self.isOpened) {
                this.closeTimeout = setTimeout(function () {
                    self._closePopup();
                }, o.hideTimeout);
            }
        },

        /**
         * Clear the timer for auto close (Works only when showOnload and hideTimeout are provided)
         */
        _clearTimeout: function () {
            var o = this.options;
            if (o.showOnload && o.hideTimeout > 0 && this.isOpened) {
                clearTimeout(this.closeTimeout);
            }
        },


        /**
         * Check if the popup is closable
         * @param {Object|null} $elem   Element that is clicked for closing
         */
        _isClosable: function ($elem) {
            var $el = $elem || null;

            // Return immidiately if no element
            if ($el === null) return true;

            // return false if closeOnOverlayClick is false
            if ( $el.hasClass('prevent-hide')) {
                return false;
            }

            // Return true only if the clicked element is close button or overlay
            if ($el.is(this.$wrapper) || $el.is(this.$closeBtn)) return true;
        },

        _setResizeEvent: function (eventType) {
            var type = eventType || 'on';
            // Trigger setPosition when resizing window or changing orientation
            if ('onorientationchange' in window) {
                $(window)[type]('orientationchange.' + namespace, $.proxy(this.setPosition, this));
            } else {
                $(window)[type]('resize.' + namespace, $.proxy(this.setPosition, this));
            }
        },

        /**
         * Handle the events on the popup
         * @param {String} eventType Type of the event that needs to be handled on the element, defaults to 'on'
         */
        _handleEvents: function (eventType) {
            var type = eventType || 'on',
                o = this.options;

            // show popup on load if it's set
            if (o.showOnload) {
                $(window)[type]('load.' + namespace, $.proxy(this._openPopup, this, true));
            }

            // Bind function for opening popup to the element
            this.$openBtn[type]('click.' + namespace, $.proxy(this.open, this));

            // Bind close button on the wrapper
            this.$wrapper[type]('click.' + namespace, $.proxy(this.close, this));

        },

    };

    $.fn.popup = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new Popup( this, options || {} )));
            }

            // Call the method on the object if it exists and it is not private
            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                data[option]();
            }
        });
    };

    $.fn.popup.Constructor = Popup;

    $.fn.popup.defaults = {
        wrapper: '.popup-wrapper', // selector(string) class for the popup wrapper
        position : 'center', // (string) center, element or null
        openBtn : '.open-popup', // (string | jQuery object) selector for popup window
        closeBtn : null,    // selector(string) additional close button
        popupOpenedButtonClass: 'popup-opened',  // (string) class added on popup open to element (button)
        closeOnOverlayClick: true, // (boolean) Is closing popup on overlay click enabled or disabled ? Default enabled
        showOnload: false, // (boolean) show popup on page load
        minTopValue: '10px', // (string | int) Minimum top position of the popup
        hideTimeout: 0, // (int) if showOnload or autoHideEveryTime is true, set the timeout for auto hiding after open
        autoHideEveryTime: false, // (boolean) Auto hide popup after hideTimeout value every time the popup is opened
        onLoaded: function () {}, // (function) Callback function when popup is setup and loaded
        onOpen: function () {}, // (function) Callback function before popup is opened, if returns false, prevents opening a popup
        onOpened: function () {}, // (function) Callback function after popup is opened
        onClose: function () {}, // (function) Callback function before popup is closed, if returns false, prevents closing a popup
        onClosed: function () {}, //  (function) Callback function after popup is closed
        ajaxOpen: false, // (function) if set, popup will open after the ajax call is done. Requires returning ajax call from function ( return $.ajax({}); )
        ajaxClose: false, // (function) if set, popup will open after the ajax call is done. Requires returning ajax call from function ( return $.ajax({}); )
    };

    // Auto initialize popup from data attribute
    var $dataPopups = $('[data-open-popup]');

    if ($dataPopups.length) {
        $dataPopups.each(function () {
            var $this = $(this);

            $('#' + $this.data('open-popup')).popup({
                openBtn: $this
            });
        });
    }

}(jQuery));
