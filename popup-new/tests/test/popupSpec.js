jasmine.getFixtures().fixturesPath = 'base/fixtures';

describe('Popup', function () {

    var popupElem;
    beforeEach(function () {
        loadFixtures('fixture.html');
        popupElem = $('#main-popup');
        jasmine.clock().install();
        // Disable jquery animations for real time testing
        $.fx.off = true;
    });

    afterEach(function () {
        jasmine.clock().uninstall();
    });

    it('Should exist on the page and be wrapped with overlay container', function () {
        popupElem.popup();
        expect(popupElem.length).toBe(1);
        expect(popupElem).toBeHidden();
        expect(popupElem.parent('.popup-wrapper')).toBeInDOM();
    });

    it('Should show on window load if "showOnload" option is provided and set to true', function () {
        popupElem.popup({
            showOnload: true
        });

        expect(popupElem).toBeHidden();

        $(window).load();

        expect(popupElem).not.toBeHidden();
    });

    it('Should show on window load if "showOnload" option is provided and hide after "hideTimeout" provided', function () {
        popupElem.popup({
            showOnload: true,
            hideTimeout: 2000
        });

        $(window).load();

        expect(popupElem).not.toBeHidden();

        jasmine.clock().tick(2001);

        expect(popupElem).toBeHidden();

    });

    it('Should hide every time when opened if the "hideTimeout" is provided and "autoHideEveryTime" is true', function () {
        popupElem.popup({
            hideTimeout: 2000,
            autoHideEveryTime: true
        }).popup('open');

        expect(popupElem).not.toBeHidden();
        jasmine.clock().tick(2001);
        expect(popupElem).toBeHidden();

        // once more
        popupElem.popup('open');
        jasmine.clock().tick(2001);
        expect(popupElem).toBeHidden();

        // Check if not closed earlier
        popupElem.popup('open');
        jasmine.clock().tick(1999);
        expect(popupElem).not.toBeHidden();
    });

    it('Should prevent closing on overlay click if "closeOnOverlayClick" option is false', function () {
        popupElem.popup({
            closeOnOverlayClick: false
        }).popup('open');

        expect(popupElem).not.toBeHidden();

        // Try clicking on overlay
        popupElem.parent().click();
        expect(popupElem).not.toBeHidden();

        // Close it on the button click
        popupElem.find('[data-close-popup]').click();
        expect(popupElem).toBeHidden();
    });

    it('Should reset popup position when resizing or changing orientation', function () {
        // Add the class for the absolute positioning
        popupElem.addClass('popup-at-element');

        popupElem.popup({
            position: 'element'
        }).popup('open');

        // Save current top
        var currentTop = popupElem.position().top;
        // Change the top so we can test if the set position worked
        popupElem.css('top', currentTop - 30);
        // Check if we changed it
        expect(currentTop).not.toEqual(popupElem.position().top);

        // Trigger resize
        $(window).resize();

        // Check if it put it back to normal
        expect(currentTop).toEqual(popupElem.position().top);
    });

    it('Should reset all cached variables and rebind events when calling "setup" method', function () {
        popupElem.popup();
        // Change the popup html so the events are lost
        popupElem.html('<h2>Changed in real time<a href="#" data-close-popup>Close popup</a></h2><p>Test</p>');
        // Open the popup
        popupElem.popup('open');
        // Make sure its opened
        expect(popupElem).not.toBeHidden();

        // Try to close it on the button click
        popupElem.find('[data-close-popup]').click();

        // Closing shouldn't work because the html is changed and events are lost
        expect(popupElem).not.toBeHidden();

        // Calling the setup method rebinds the events and recaches the variables
        popupElem.popup('setup');
        // Calling close button click again
        popupElem.find('[data-close-popup]').click();
            // This time the popup should be closed
        expect(popupElem).toBeHidden();
    });



    it('Should delete plugin from element when "destroy" method is called', function () {
        popupElem.popup();
        // Make sure it works
        $('.open-popup').click();
        expect(popupElem).not.toBeHidden();
        popupElem.find('[data-close-popup]').click();
        expect(popupElem).toBeHidden();

        // Now we destroy it and check if everything is deleted
        popupElem.popup('destroy');

        // Check if it hides
        expect(popupElem).toBeHidden();
        // Check if it doesn't open on button click
        $('.open-popup').click();
        expect(popupElem).toBeHidden();
        // Check if data was removed
        expect(popupElem.data('popup')).toBeUndefined();
    });


    describe('Should be possible', function () {
        beforeEach(function () {
            popupElem.popup();
        });

        it('to open with "open" method called as parameter', function () {
            popupElem.popup('open');
            expect(popupElem).not.toBeHidden();
        });

        it('to close with "close" method called as parameter', function () {
            popupElem.popup('open');
            expect(popupElem).not.toBeHidden();
            popupElem.popup('close');
            expect(popupElem).toBeHidden();
        });

        it('to open on clicking the button associated with it', function () {
            $('.open-popup').click();
            expect(popupElem).not.toBeHidden();
        });

        it('to close on clicking the close button inside of it', function () {
            $('.open-popup').click();

            expect(popupElem).not.toBeHidden();

            popupElem.find('[data-close-popup]').click();

            expect(popupElem).toBeHidden();
        });

        it('to close on clicking the overlay', function () {
            $('.open-popup').click();

            expect(popupElem).not.toBeHidden();

            popupElem.parent().click();

            expect(popupElem).toBeHidden();
        });
    });


    describe('Should handle callback methods', function () {
        var testFunctionOpen,
            testFunctionOpened,
            testFunctionClose,
            testFunctionClosed;

        beforeEach(function () {
            testFunctionLoaded = jasmine.createSpy('testFunctionLoaded');
            testFunctionOpen = jasmine.createSpy('testFunctionOpen');
            testFunctionOpened = jasmine.createSpy('testFunctionOpened');
            testFunctionClose = jasmine.createSpy('testFunctionClose');
            testFunctionClosed = jasmine.createSpy('testFunctionClosed');

            popupElem.popup({
                onLoaded: testFunctionLoaded,
                onOpen: testFunctionOpen,
                onOpened: testFunctionOpened,
                onClose: testFunctionClose,
                onClosed: testFunctionClosed
            });
        });

        it('and call onLoaded event when initialized', function () {
            expect(testFunctionLoaded).toHaveBeenCalled();
        });

        it('and call onOpen and onOpened function on opening a popup', function () {
            popupElem.popup('open');
            expect(testFunctionOpen).toHaveBeenCalled();
            expect(testFunctionOpened).toHaveBeenCalled();
        });

        it('and call onClose and onClosed function on closing a popup', function () {
            popupElem.popup('open').popup('close');

            expect(testFunctionClose).toHaveBeenCalled();
            expect(testFunctionClosed).toHaveBeenCalled();
        });

    });

    describe('Should trigger custom event', function () {

        it('"loaded.popup" when popup is initialized', function () {
            spyOnEvent('#main-popup', 'loaded.popup');

            popupElem.popup();

            expect('loaded.popup').toHaveBeenTriggeredOn('#main-popup');
        });

        it('"open.popup" and "opened.popup" when popup is opened', function () {
            spyOnEvent('#main-popup', 'open.popup');
            spyOnEvent('#main-popup', 'opened.popup');

            popupElem.popup().popup('open');

            expect('open.popup').toHaveBeenTriggeredOn('#main-popup');
            expect('opened.popup').toHaveBeenTriggeredOn('#main-popup');
        });

        it('"close.popup" and "closed.popup" when popup is closed', function () {
            spyOnEvent('#main-popup', 'close.popup');
            spyOnEvent('#main-popup', 'closed.popup');

            popupElem.popup().popup('open').popup('close');

            expect('close.popup').toHaveBeenTriggeredOn('#main-popup');
            expect('closed.popup').toHaveBeenTriggeredOn('#main-popup');
        });

    });


    describe('Should wait for', function () {

        beforeEach(function () {
            spyOn($, 'ajax').and.callFake(function () {
                var d = $.Deferred();
                setTimeout(function() {
                    d.resolve('sir');
                }, 2000);
                return d;
            });

        });

        it('ajaxOpen to finish loading and open after that', function () {

            popupElem.popup({
                ajaxOpen: function () {
                    return $.ajax();
                }
            }).popup('open');

            jasmine.clock().tick(500);
            expect(popupElem.css('opacity')).toEqual('0');
            jasmine.clock().tick(2000);
            expect(popupElem.css('opacity')).toEqual('1');
        });

        it('ajaxClose to finish loading and close after that', function () {
            popupElem.popup({
                ajaxClose: function () {
                    return $.ajax();
                }
            }).popup('open');

            expect(popupElem.css('opacity')).toEqual('1');

            popupElem.popup('close');

            jasmine.clock().tick(500);
            expect(popupElem.css('opacity')).toEqual('1');
            jasmine.clock().tick(2000);
            expect(popupElem.css('opacity')).toEqual('0');
        });

    });

});
