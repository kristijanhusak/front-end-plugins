/*
 * koapopup
 *
 *
 * Copyright (c) 2012 Kristijan Husak & Aleksandar Gosevski
 * Licensed under the MIT, GPL licenses.
 */

(function($) {
    "use strict";
    var namespace = 'popup';

    function Popup(element, options) {
        if (!element) return null;
        var self = this, o = this.options = $.extend({}, $.fn.popup.defaults, options);
        // main element
        this.$element = $(element);
        this.isOpened = false;
        // Callback functions
        this.callbackBeforeOpen = o.callbackBeforeOpen || function () {};
        this.callbackOpened = o.callbackOpened || function () {};
        this.callbackBeforeClosed = o.callbackBeforeClosed || function () {};
        this.callbackClosed = o.callbackClosed || function () {};
        this.ajaxFunction = o.ajaxFunction || false;

        this._setupPopupWindow();

        this._appendOverlay();

        this._bindEvents();

    }

    Popup.prototype = {

        /**
         * Opens the popup. If ajax function exists, executes it and calls opening after it finishes
         */
        open: function(e) {
            e && e.preventDefault();

            if (this.ajaxFunction !== false) {
                this.ajaxCall();
            } else {
                this._openPopup();
            }
        },

        /**
         * Close the popup
         */
        close: function(e) {
            e && e.preventDefault();
            this._closePopup(e && $(e.currentTarget));
        },

        /**
         * Calls ajax function if exists and triggering open popup when its finishes
         */
        ajaxCall: function() {
            this.ajaxFunction(this.$element)
                .done($.proxy(this._openPopup, this));
        },

        /**
         * Triggered when the window is resized or orientation is changed
         */
        resize : function () {
            this.$overlay.css({
                width: $(document).width(),
                height: $(document).height()
            });

            this.$popup.css({
                'max-width' : $(document).width()
            });

            this._setPosition();
        },

        /**
         * Does popup window setup when "position" option is set to "element"
         */
        _positionElement: function () {
            var position = this.$element.offset();

            this.$popup.css({
                top : position.top,
                left : position.left
            });
        },

        /**
         * Position the popup to the center of the screen
         */
        _positionCenter: function () {
            var o = this.options,
                topVal = window.innerHeight/2 - this.$popup.innerHeight()/2;

            this.$popup.css({
                top : ((topVal < 0) ? '10px' : topVal),
                left : window.innerWidth/2 - this.$popup.innerWidth()/2
            });
        },

        /**
         * Handles calling proper positioning method
         */
        _setPosition: function () {
            var o = this.options;

            if (o.position === 'element') {
                this._positionElement();
            } else if (o.position === 'center') {
                this._positionCenter();
            }

            this.$popup.animate({opacity: 1}, 200);
        },

        /**
         * Opens the popup window and shows the overlay
         */
        _openPopup: function(onload) {
            var self = this, o = this.options;
            // Function called before popup open
            this.callbackBeforeOpen(this.$element);

            this._showOverlay();
            this.$popup.fadeIn('fast', function () {
                // Function called after popup is opened
                self.callbackOpened(self.$element);
                self.isOpened = true;
                self._setPosition();
                self.$element.addClass(o.popupOpenedButtonClass);

                // set timeout for auto hide if options are set that way
                if (onload || o.autoHideEveryTime) {
                    self._hideTimeout();
                }

            });
        },

        /**
         * Calls the close method after the given timeout from options
         */
        _hideTimeout: function () {
            var self = this, o = this.options;
            if (o.hideTimeout > 0 && self.isOpened) {
                this.closeTimeout = setTimeout(function () {
                    self._closePopup();
                }, o.hideTimeout);
            }
        },

        /**
         * Clear the timer for auto close
         */
        _clearTimeout: function () {
            var o = this.options;
            if (o.showOnload && o.hideTimeout > 0 && this.isOpened) {
                clearTimeout(this.closeTimeout);
            }
        },

        /**
         * Closes the popup and hides overlay
         */
        _closePopup: function ($elem) {
            var self = this, o = this.options,
                $el = $elem || null,
                closable = ($el !== null) ? !$el.hasClass('prevent-hide') : true;

            // If element that is clicked for closing has an option to disable
            if (!closable) return false;

            // Clearing timer for auto hide timeout
            this._clearTimeout();
            // Function called before popup close
            this.callbackBeforeClosed(this.$element);

            this.$popup.fadeOut(200,function () {
                $(this).css('opacity', 0);
                // Function called after popup is closed
                self.callbackClosed(self.$element);
                self.isOpened = false;
                self.$element.removeClass(o.popupOpenedButtonClass);
                self._hideOverlay();
            });
        },

        _appendOverlay: function () {
            var o = this.options;

            if (!$('.' + o.overlayClass).length) {
                $('body').append( $('<div>', { 'class': o.overlayClass }).css( o.overlay ).hide() );
            }

            this.$overlay = $('.' + o.overlayClass);
        },
        _showOverlay: function () {
            var o = this.options;
            // If we want to prevent closing popup on overlay, we put a class on it
            if (!o.hidePopupOnOverlay) {
                this.$overlay.addClass('prevent-hide');
            } else {
                this.$overlay.removeClass('prevent-hide');
            }

            this.$overlay.css('height', $(document).height()).show();
        },

        _hideOverlay: function () {
            this.$overlay.fadeOut(200, function () {
                $(this).removeClass('prevent-hide');
            });
        },

        _setupPopupWindow: function () {
            var o = this.options;
            // If the data attribute exists, use that as popup window
            if (this.$element.data('popup-window')) {
                o.popupWindow = '#' + this.$element.data('popup-window');
            }

            // Set required popup css
            this.$popup = $(o.popupWindow).css({
                position: 'fixed',
                'z-index' : '999999',
                'display' : 'none',
                'max-width' : $(document).width(),
                '-webkit-transform': 'translate3d(0,0,0)',
                'opacity': 0
            });
        },

        _bindEvents: function () {
            var o = this.options;
            // show popup on load if it's set
            if (o.showOnload) {
                $(window).on('load', $.proxy(this._openPopup, this, true));
            }

            // Bind function for opening popup to the element
            this.$element.off('click.' + namespace, $.proxy(this.open, this))
                        .on('click.' + namespace, $.proxy(this.open, this));

            // If the option for hiding popup on overlay click is enabled, bind close event to it
            if (o.hidePopupOnOverlay) {
                this.$overlay.on('click.' + namespace, $.proxy(this.close, this));
            }

            // If close button is provided, attach close event to it
            if (o.closeBtn !== null) {
                $(o.closeBtn).on('click.' + namespace, $.proxy(this.close, this));
            }

            // Setup position and overlay on window resize
            if( 'onorientationchange' in window) {
                $(window).on('orientationchange', $.proxy(this.resize, this));
            } else {
                $(window).on('resize', $.proxy(this.resize, this));
            }
        }

    };

    $.fn.popup = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new Popup(this, options)));
            }

            // Call the method on the object if it exists and it is not private
            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                data[option]();
            }
        });
    };

    $.fn.popup.Constructor = Popup;
    $.fn.popup.defaults = {
        position : 'center', // center, element or null
        popupWindow : '.popup-box', // selector for popup window
        closeBtn : '.close-popup',    // additional close button
        popupOpenedButtonClass: 'popup-opened', // class added on popup open to element (button)
        overlayClass: 'popup-overlay', // class for the overlay
        hidePopupOnOverlay: true, // Is closing popup on overlay click enabled or disabled ? Default enabled
        showOnload: false, // show popup on page load
        hideTimeout: 0, // if showOnload or autoHideEveryTime is true, set the timeout for auto hiding after open
        autoHideEveryTime: false, // Auto hide popup after hideTimeout value every time the popup is opened
        callbackBeforeOpen: function () {}, // Callback function before popup is opened
        callbackOpened: function () {}, // Callback function after popup is opened
        callbackBeforeClosed: function () {}, // Callback function before popup is closed
        callbackClosed: function () {}, // Callback function after popup is closed
        ajaxFunction: false, // if set, popup will open after the ajax call is done. Requires returning ajax call from function ( return $.ajax({}); )
        overlay: {
            left: 0,
            top: 0,
            width: $(document).width(),
            height: $(document).height(),
            position: 'fixed',
            background: 'rgba(0,0,0,.4)',
            userSelect: 'none',
            zIndex: '99999'
        }
    };

}(jQuery));
