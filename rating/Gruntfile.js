module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        concat: {
            dist: {
                src: ['src/koarating.js'],
                dest: 'dist/koarating.js'
            }
        },

        uglify: {
            project_production: {
                files: {
                    'dist/koarating.min.js': ['dist/koarating.js']
                }
            }
        },

        watch: {
            files: ['src/koarating.js'],
            tasks: ['concat', 'uglify']
        }

    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['concat', 'uglify']);

};
