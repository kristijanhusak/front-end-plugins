$(document).ready(function () {
  var $searchInput = $('.search'),
      $searchTable = $('.search-table');

  $searchInput.on('keyup', function () {
    var searchText = $(this).val();
    if (searchText.length > 0) {
      $searchTable.find('tr').hide();
      $searchTable.find('td:contains-ci(' + searchText + ')')
              .parent('tr').show();
    } else {
      $searchTable.find('tr').show();
    }
  });
});


$.extend($.expr[":"],
{
    "contains-ci": function(elem, i, match, array)
    {
        return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});
