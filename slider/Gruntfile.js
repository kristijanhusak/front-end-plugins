module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        concat: {
            dist: {
                src: ['src/koaSlider.js'],
                dest: 'src/koaSlider.jquery.js'
            }
        },

        uglify: {
            project_production: {
                files: {
                    'src/koaSlider.jquery.min.js': ['src/koaSlider.jquery.js']
                }
            }
        },

        watch: {
            files: ['src/koaSlider.js'],
            tasks: ['concat', 'uglify']
        }

    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['concat', 'uglify']);

};
