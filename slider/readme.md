# koaSlider

Responsive slider with infinite scrolling, navigation.

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/kristijanhusak/koaSlider/master/dist/koaSlider.min.js
[max]: https://raw.github.com/kristijanhusak/koaSlider/master/dist/koaSlider.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/koaSlider.min.js"></script>
<script>
jQuery(function($) {
  $.awesome(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
