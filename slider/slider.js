if ( typeof Object.create !== 'function' ) {
	Object.create = function( obj ) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

(function ( $, window, document, undefined) {
	var koaSlider = {
		init: function (options, container) {

			var self = this;
			self.options = $.extend({}, $.fn.koaSlider.options, options);
			self.container = container;
			self.$container = $(container).css('opacity', 0);
			
			self.slider = self.container.children[0];

			self.$slider = self.$container.find('ul').css({ 'list-style' : 'none', 'margin' : '0', 'padding' : '0', 'position' : 'relative', 'left' : 0});
			self.$slides = self.$slider.children().css({'display': 'block', 'float' : 'left', 'box-sizing' : 'content-box'});

			if (self.options.half) {

					self.$slides.last().clone(true).prependTo(self.$slider);
					self.$slides.last().prev('li').clone(true).prependTo(self.$slider);

					self.$slides.first().clone(true).appendTo(self.$slider);
					self.$slides.first().next('li').clone(true).appendTo(self.$slider);

			} else {
				self.$slides.last().clone(true).prependTo(self.$slider);
				self.$slides.first().clone(true).appendTo(self.$slider);
			}

			self.current = 1;
			self.setup();
			self.slide(1);

			if (self.options.pagination) {
				for (var i = 0; i < (self.countSlides - (self.options.half ? 4 : 2)); i++ ) {
					var paginationText = (self.options.paginationText === 'number') ? i + 1 : '&bull;';
					self.options.pagination.append('<a href="#"' + ((i === 0) ? ' class="active"' : '') + '>' + paginationText + '</a>');
				}
			}

			setTimeout(function () {
				self.$container.animate({'opacity': 1});
			}, self.options.speed);
			
			self.slider.addEventListener('touchstart', $.proxy(self.touchStart, this), false);
			self.slider.addEventListener('touchmove', $.proxy(self.touchMove, self), false);
			self.slider.addEventListener('touchend', $.proxy(self.touchEnd, self), false);
			window.addEventListener('resize', $.proxy(self.setup, self), false);

			if(self.options.nextBtn)
				self.options.nextBtn.on('click', function (e) {
					e.preventDefault();
					self.slide(self.current + 1, 'next');
				});

			if(self.options.prevBtn)
				self.options.prevBtn.on('click', function (e) {
					e.preventDefault();
					self.slide(self.current - 1, 'prev');
				});

			if (self.options.pagination) {
				self.options.pagination.on('click', 'a', function (e) {
					e.preventDefault();
					var index = $(this).index() + 1;
					self.slide(index);
				});
			}
		},

		setup: function () {
			var self = this;

			self.containerWidth = self.$container.innerWidth();
			self.countSlides = self.$slider.children().length;
			self.slideValue = self.options.half ? self.containerWidth/self.options.halfValue : self.containerWidth;
			self.$slider.width(self.containerWidth * self.countSlides).css('margin-left', self.options.half ? (-self.containerWidth/(2*self.options.halfValue)) : 0);
			self.$slider.children().outerWidth(self.slideValue);
			self.slide(self.current);

		},

		infinite: function(pos, dir) {
			var self = this;

			if (dir === 'next') {
				if ((self.countSlides - (self.options.half ? 3 : 1)) === pos) {
					self.current = 1;
					return true;
				}
				else {
					self.current = pos;
					return false;
				}
			}
			else if (dir === 'prev') {
				if (pos === 0) {
					self.current = self.countSlides - (self.options.half ? 4 : 2);
					return true;
				}
				else {
					self.current = pos;
					return false;
				}
			}
			else {
				self.current = pos;
				return false;
			}
		},

		slide: function (pos, dir) {
			var self = this;

				self.infinite(pos, dir);

				self.options.callbackBefore(self.current);

				if (self.options.pagination) {
					self.options.pagination.children().eq(self.current - 1).addClass('active').siblings().removeClass('active');
				}

					self.$slider.animate({'left': - (pos * self.slideValue)}, self.options.speed, function () {

						if (self.infinite(pos, dir))
							self.$slider.css('left', -(self.current * self.slideValue));

						self.$slides.eq(self.current - 1).addClass(self.options.activeClassName).siblings().removeClass(self.options.activeClassName);

						self.options.callbackAfter(self.current);
					});
		},

		touchStart: function (e) {
			var self = this;

			self.deltaX = 0;
			self.touchX = e.touches[0].pageX;
			self.currLeft = parseInt(self.$slider.css('left'), 10);
			self.startTime = Number(new Date());
			

			e.stopPropagation();
		},

		touchMove: function (e) {
			var self = this;
			self.touchxMove =  e.touches[0].pageX - self.touchX;

			self.$slider.css('left', (self.currLeft + self.touchxMove) );

			e.preventDefault();
			e.stopPropagation();
		},

		touchEnd: function (e) {
			var self = this,
				o = self.options,

			isValidSlide = Number(new Date()) - self.startTime < 250 && Math.abs(self.touchxMove) > 20 || Math.abs(self.touchxMove) > self.containerWidth/3;

			

			if (self.touchxMove < 0 && isValidSlide) {
				self.current++;
				self.slide(self.current, 'next');
			}
			else if (self.touchxMove > 0 && isValidSlide) {
				self.current--;
				self.slide(self.current, 'prev');
			}
			else
				self.slide(self.current);
			
			
			e.stopPropagation();
		}
	};

	$.fn.koaSlider = function(options) {
		this.each(function () {
			var slider = Object.create(koaSlider);
			slider.init(options, this);

			$.data(this, 'koaSlider', slider);
		});
	};

	$.fn.koaSlider.options = {
		activeClassName: 'active',
		half: false,
		speed: 300,
		halfValue: 2,
		nextBtn: null,
		prevBtn : null,
		pagination: false,
		paginationText : 'number', // dots or numbers
		callbackBefore: function () {},
		callbackAfter: function () {}
	};

})(jQuery, window, document, undefined);