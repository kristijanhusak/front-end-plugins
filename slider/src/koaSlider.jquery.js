/*
 * koaSlider
 * https://github.com/kristijanhusak/koaSlider
 *
 * Copyright (c) 2013 Kristijan Husak
 * Licensed under the MIT license.
 */

(function ( $ ) {
    "use strict";
    var namespace = 'koaslider',
        KoaSlider;

    KoaSlider = function (options, container) {
            // If there is no element null is returned
            if (!container) return null;

            var self = this,
                i, paginationText, lastSlides, firstSlides, o, countCheck;

            self.options = o = $.extend( {}, $.fn.koaSlider.options, options );

            // setting correct halfValue if half option is false
            self.options.halfValue = (!o.half && o.halfValue != 2) ? 2 : o.halfValue;

            self.$container = $(container).css({'-webkit-transform' : 'translate3d(0, 0, 0)', '-webkit-backface-visibility' : 'hidden'});
            self.slider = container.children[0];
            self.touchEnabled = ('ontouchstart' in window || 'onmsgesturechange' in window) ? true : false;
            // Flag for current slide position (if circular is true)
            self.FirstOrLastSlide = false;

            if (typeof self.options.pagination == 'string' && self.options.pagination != false) {
                self.options.pagination = self.$container.find(self.options.pagination);
            }

            if (typeof self.options.prevBtn == 'string' && self.options.prevBtn != null) {
                self.options.prevBtn = self.$container.find(self.options.prevBtn);
            }

            if (typeof self.options.nextBtn == 'string' && self.options.nextBtn != null) {
                self.options.nextBtn = self.$container.find(self.options.nextBtn);
            }

            // Check if css transitions and transforms are supported, if no, we will use jquery animation
            self.supportTransition = ( self.checkTransformSupport() && self.checkTransitionSupport() );

            self.$slider = self.$container.children().first().css( {'opacity': 0, 'list-style' : 'none', 'margin' : '0', 'padding' : '0', 'position' : 'relative', 'left' : 0, '-webkit-backface-visibility' : 'hidden'} );
            self.$slides = self.$slider.children().css( {'display': 'block', 'float' : 'left', '-webkit-backface-visibility' : 'hidden'} );
            self.countSlides = self.realSlidesCount = self.$slides.length;

            // Hide or show buttons if the touch is enabled and options for hiding buttons is set to true
            self.showButtons = self.touchEnabled && o.hideBtnOnTouch;
            countCheck = (!o.half) ? (o.halfValue - 1) : o.halfValue;
            // Cloning elements for circular option
            if (o.circular && self.realSlidesCount > countCheck) {
                if (self.options.half) {
                    lastSlides = firstSlides = self.$slides;
                    lastSlides = lastSlides.slice(-o.halfValue).clone(true).prependTo(self.$slider);
                    firstSlides = firstSlides.slice(0, o.halfValue).clone(true).appendTo(self.$slider);
                } else {
                    self.$slides.last().clone(true).prependTo(self.$slider);
                    self.$slides.first().clone(true).appendTo(self.$slider);
                }

            }

            self.slideIndex = self.current = o.circular ? (o.half ? o.halfValue - o.halfValue/2 : 1) : 0; // first slide depending on options

            // Initial setup call
            if (self.realSlidesCount > countCheck) {
                self.setup();
            }

            // Animate opacity and remove max-height on load
            $(document).ready(function () {
                self.$container.removeClass('loading')
                self.$slider.css('max-height','none');
                self.$slider.delay(o.fadeInSpeed).animate( { opacity: 1 } );
            });

            // If there no enough slides, reset setup
            if (self.realSlidesCount <= countCheck) {
                setTimeout(function () {
                    self.destroy(true);
                }, (o.fadeInSpeed + 100));

                return null;
            }

            // Set pagination
            if ( o.pagination ) {
                self._setPagination();
            }

             // Bind next button event
            if( o.nextBtn ) {
                self._setNavigationButton(o.nextBtn, 'next');
            }

            // Bind prev button event
            if( o.prevBtn ) {
                self._setNavigationButton(o.prevBtn, 'prev');
            }

            // Bind events
            if (self.touchEnabled && o.enableTouch) {
                self.slider.addEventListener( 'touchstart', $.proxy( self._touchStart, self ), false );
                self.slider.addEventListener( 'touchmove', $.proxy( self._touchMove, self ), false );
                self.slider.addEventListener( 'touchend', $.proxy( self._touchEnd, self ), false );
            }

            // Call setup method on resize
            if( 'onorientationchange' in window) {
                $(window).on('orientationchange', $.proxy(self.setup, self));
            } else {
                $(window).on('resize', $.proxy(self.setup, self));
            }
    };

    KoaSlider.prototype = {
        // Public methods
        setup: function () {
            var self = this;
            self.containerWidth = self.$container.width(); // Slider container width
            self.evenHalfValue = (self.options.halfValue % 2 === 0) ? 2 : 1; // Checking the half value option for the cutting first and last slide
            self.countSlides = self.$slider.children().length; // Number of slides
            self.slideValue = self.options.half ? parseInt(self.containerWidth/self.options.halfValue, 10) : self.containerWidth; // Calculate how much slider needs to move on slide
            // Setting margin left depending on options(cutFirstAndLast)
            if (self.options.circular) {
                self.realSlidesCount = self.options.half ? (self.countSlides - 2*self.options.halfValue) : (self.countSlides - 2); // Real number of slides
                self.marginLeftValue = self.options.half ? ( self.options.cutFirstAndLast ? (-self.containerWidth/self.options.halfValue/self.evenHalfValue ) : (-self.containerWidth/2) ) : 0
            } else {
                self.realSlidesCount = self.countSlides;
                self.marginLeftValue = self.options.half ? (self.options.cutFirstAndLast ? (self.containerWidth/2 - self.slideValue/2) : 0 ) : 0;
            }

            self.$slider.outerWidth(self.slideValue * self.countSlides).css( 'margin-left', self.marginLeftValue); // Setting width for slides container (ul)
            self.$slider.children().outerWidth(self.slideValue); // setting slide width
            self.slideTo( self.current ); // first initial slide
            self.isSliding = false; // setting sliding to false
            // Setting automatic sliding if enabled by options
            self._setAutoSlide();

        },

        slideToNext: function () {
            this._slide(++this.current, 'next');
        },

        slideToPrev: function () {
            this._slide(--this.current, 'prev');
        },

        slideTo: function (index) {
            this._slide(index, 'next');
        },

        // Check if current slide is first
        isFirst: function () {
            var self = this, o = self.options;
            if (o.circular) {
                return ( self.current === ( o.half ? -1 : 0) );
            } else {
                return ( self.current === 0);
            }
        },

        // Check if current slide is last
        isLast: function () {
            var self = this, o = self.options;
            if (o.circular) {
                return ( self.countSlides === ( self.current + (o.half ? 2 : 1) ) );
            } else {
                if (o.cutFirstAndLast) {
                    return (self.current == (self.countSlides - 1));
                } else {
                    return (self.current == (self.countSlides - (o.halfValue) + 1));
                }
            }

        },

        currentSlideIndex: function () {
            return this.current;
        },

        checkTransformSupport: function () {
            var prefixes = 'transform WebkitTransform MozTransform OTransform msTransform'.split(' '), i;
            for(i = 0; i < prefixes.length; i++) {
                if(document.createElement('div').style[prefixes[i]] !== undefined) {
                    return true;
                }
            }
            return false;
        },

        checkTransitionSupport: function () {
            var prefixes = 'transition WebkitTransition MozTransition OTransition MsTransition'.split(' '), i;
            for(i = 0; i < prefixes.length; i++) {
                if(document.createElement('div').style[prefixes[i]] !== undefined) {
                    return true;
                }
            }
            return false;
        },

        destroy: function (triggerCallbacks) {
            var self = this,
                o = this.options,
                triggerCallbacks = triggerCallbacks || false;

            self.realSlidesCount = 1;

            if (triggerCallbacks) {
                o.callbackBefore(self.$slides[0], self);
            }


            self.$slider.css(self._getTransitionValues(0, '0s')).css({ 'margin-left': 0, 'left' : 0 });

            if (o.prevBtn) o.prevBtn.hide();

            if (o.nextBtn) o.nextBtn.hide();

            if (o.pagination) o.pagination.hide();

            clearInterval(self.autoSlide);

            if (triggerCallbacks) {
                o.callbackAfter(self.$slides[0], self);
            }
        },

        // Private methods
        _setAutoSlide: function () {
            var self = this, o = self.options;

            if (o.auto > 0 && o.circular) {
                clearInterval(self.autoSlide);
                self.autoSlide =  setInterval(function () {
                    self.slideToNext();
                }, o.auto);
            }
        },

        _setCurrentSlideIndex: function(pos, dir) {
            var self = this, o = self.options;

            if (o.circular) {
                if (dir === 'next') {
                    if ( ( self.countSlides - ( o.half ? (o.halfValue + o.halfValue/2) : 1) ) === pos ) {
                        self.current = self.slideIndex;
                        self.FirstOrLastSlide = true;
                        return;
                    }
                }
                if (dir === 'prev') {
                    if (pos === (self.slideIndex - 1)) {
                        self.current = self.countSlides - ( o.half ? (o.halfValue + o.halfValue/2 + 1) : 2 );
                        self.FirstOrLastSlide = true;
                        return;
                    }
                }
            }

            self.current = pos;
            self.FirstOrLastSlide = false;
            return;
        },

        _slide: function (pos, dir) {

            var self = this, animation,
                o = self.options,
                speed = o.speed / 1000 + 's';

            // Set flag to prevent sliding until current is finished
            self.isSliding = true;

            self._setCurrentSlideIndex(pos, dir);

            // Callback function before sliding
            o.callbackBefore( self.$slides[self.current - (self.slideIndex)], self );

            // Change current active pagination
            if (o.pagination) {
                self._setPaginationActiveClass();
            }

            animation = - ( pos * self.slideValue );

            // Sliding with css animations if supported
            if (self.supportTransition) {
                self._slideWithCssTransition(animation, speed);
            } else { // otherwise, slide with js animation
                self._slideWithJSAnimation(animation, speed);
            }

            // Set button visibility depending on options
            self._setButtonVisibility();

        },

        _slideWithCssTransition: function(slideTo, speed) {
            var self = this;

            self.$slider.css( self._getTransitionValues( slideTo, speed ) );

            self.$slider.one('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function () {
                // Set starting or ending position if circular is true
                if ( self.FirstOrLastSlide ) {
                    self.$slider.css( self._getTransitionValues( -(self.current * self.slideValue), '0s' ) );
                }
                self._afterSliding();
            });
        },

        _slideWithJSAnimation: function (slideTo, speed) {
            var self = this;
            self.$slider.stop().animate( {'left': slideTo }, self.options.speed, function () {
                // Slide to beginning or end if circular is true
                if ( self.FirstOrLastSlide ) {
                    self.$slider.css( 'left', -(self.current * self.slideValue) );
                }
                self._afterSliding();
            });
        },

        _getTransitionValues: function (value, speed) {
            return {
                WebkitTransform : 'translate3d(' + value + 'px, 0, 0)',
                MozTransform : 'translate3d(' + value + 'px, 0, 0)',
                msTransform : 'translateX(' + value + 'px) translateZ(0)',
                OTransform : 'translateX(' + value + 'px)',
                transform : 'translateX(' + value + 'px) translateZ(0)',
                WebkitTransition : '-webkit-transform ' + speed ,
                MozTransition : '-moz-transform ' + speed ,
                MsTransition : '-ms-transform ' + speed ,
                OTransition : '-o-transform ' + speed ,
                transition : 'transform ' + speed
            };
        },

        _afterSliding: function () {
            var self = this, o = self.options;

            self.isSliding = false;
            self.FirstOrLastSlide = false;
            // Add active class to current slide
            self._setSlideActiveClass();
            // Callback function after sliding is finished
            o.callbackAfter(self.$slides[self.current - (self.slideIndex)], self );

        },

        _setPaginationActiveClass: function (i) {
            var self = this, o = self.options, index = i || self.current;
            o.pagination
                    .children().eq( index - (self.slideIndex) )
                        .addClass(o.paginationActiveClassName)
                    .siblings()
                        .removeClass(o.paginationActiveClassName);
        },

        _setSlideActiveClass: function (i) {
           var self = this, o = self.options, index = i || self.current;
           self.$slides.eq( index - (self.slideIndex) ).addClass( o.slideActiveClassName ).siblings().removeClass( o.slideActiveClassName );
        },

        _setButtonVisibility: function () {
            var self = this, o = self.options;
            if ( o.nextBtn ) {
                if ( ( !self.circular && self.isLast() ) || self.showButtons ) {
                    o.nextBtn.hide();
                } else {
                    o.nextBtn.show();
                }
            }

            if ( o.prevBtn ) {
                if ( !self.circular && self.isFirst() || self.showButtons ) {
                    o.prevBtn.hide();
                } else {
                    o.prevBtn.show();
                }
            }
        },

        _touchStart: function (e) {
            var self = this;
            if (self.isSliding) return;
            self.touchX = e.touches[0].pageX;
            self.touchxMove = 0;
            self.currLeft = -(self.current * self.slideValue);
            self.startTime = Number( new Date() );
            clearInterval(self.autoSlide);
            self.slider.addEventListener( 'touchmove', $.proxy( self._touchMove, self ), false );
            self.slider.addEventListener( 'touchend', $.proxy( self._touchEnd, self ), false );
        },

        _touchMove: function (e) {
            // return if sliding flag is set to true
            if (this.isSliding || e.touches.length > 1 || e.scale && e.scale !== 1) return;

            var self = this, slideAmount, moved = e.touches[0].pageX - self.touchX;

            self.touchxMove =  moved/(self.options.halfValue/2.2), slideAmount = self.currLeft + self.touchxMove;

            clearInterval(self.autoSlide);

            if (self.supportTransition) {
                self.$slider.css(self._getTransitionValues( slideAmount ,0));
            } else {
                self.$slider.css('left', slideAmount );
            }

            if (Math.abs(moved) > 5) {
                e.preventDefault();
            }

        },

        _touchEnd: function (e) {
            // return if sliding flag is set to true
            if (this.isSliding) return;

            var self = this,
                o = self.options,
                isValidSlide, isLastOrFirst, circular;

            // Check if we have any slider movement
            if (self.touchxMove != 0) {
                isLastOrFirst = ( self.isLast() && self.touchxMove < 0 )  || ( self.isFirst() && self.touchxMove > 0 );
                isValidSlide = ( Number( new Date() ) - self.startTime < 250 && Math.abs( self.touchxMove ) > 20 ) || (Math.abs( self.touchxMove ) > self.containerWidth/( o.half ? (o.halfValue * 3) : 4 ));
                circular = o.circular ? false : isLastOrFirst;

                if (isValidSlide && !circular) {
                    (self.touchxMove > 0) ? self.slideToPrev() : self.slideToNext();
                } else {
                    self._slide( self.current );
                }

                self.slider.removeEventListener( 'touchmove', $.proxy( self._touchMove, self ), false );
                self.slider.removeEventListener( 'touchend', $.proxy( self._touchEnd, self ), false );

                // Prevent default only if scrolling horizontally
                if (Math.abs(self.touchxMove) > 3) {
                    e.preventDefault();
                }
            }
            self._setAutoSlide();

        },

        _setPagination: function () {
            var count, self = this, o = self.options, i, paginationText;
            count = o.circular ? ( self.countSlides - (o.half ? o.halfValue * 2 : 2) ) : self.countSlides;
            for ( i = 0; i < count; i++ ) {
                paginationText = ( o.paginationText === 'numbers' ) ? i + 1 : '&bull;';
                o.pagination
                    .append('<a href="#"' + ((i === 0) ? ' class="' + o.paginationActiveClassName + '"' : '') + '>' + paginationText + '</a>');
            }

            self.options.pagination.on('click.' + namespace, 'a', function (e) {
                e.preventDefault();
                var $this = $(this), index = $this.index() + self.slideIndex;
                self.slideTo( index );
                self._setAutoSlide();
            });
        },

        _setNavigationButton: function (el, dir) {
            var self = this, afterSlideNum = (dir === 'next') ? 1 : -1,
            prevOrNext;
            el.on('click.' + namespace, function (e) {
                e.preventDefault();
                if (self.isSliding) return;
                prevOrNext = (dir === 'next') ? self.isLast() : self.isFirst();

                if ( self.options.circular ) {
                    self._slide( self.current + afterSlideNum, dir );
                } else {
                    if ( prevOrNext) {
                        return false;
                    } else {
                        self._slide( self.current + afterSlideNum, dir );
                    }
                }
                self._setAutoSlide();
                e.stopImmediatePropagation();
            });
        }
    };

    $.fn.koaSlider = function(option) {
        // any data returned from methods called through plugin
        var returnData;

        this.each(function () {
            returnData = null;
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new KoaSlider(options, this)));
            }

            // Call the method on the object if it exists and it is not private
            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                // If the string contains colon (:), first chunk is method, and second chunk is a parameter
                if (option.indexOf(':') != -1) {
                    var optionArr = option.split(':');
                    returnData = data[optionArr[0]](optionArr[1]);
                } else {
                    returnData = data[option]();
                }
            }
        });

        if (returnData !== null) {
            return returnData;
        }
    };

    $.fn.koaSlider.Constructor = KoaSlider;

    $.fn.koaSlider.options = {
        circular: true,                         // Boolean - Should slider be circular
        auto: 0,                                // Number - Value in ms for auto sliding
        enableTouch: true,                      // Boolean - Enable touch events by default
        speed: 400,                             // Integer - Speed of slides transition
        slideActiveClassName: 'active',         // String - Class name for current slide list element (li)
        paginationActiveClassName: 'active',    // String - Class name for current active pagination
        half: false,                            // Boolean - if true, shows "halfValue" numbers of slides
        halfValue: 2,                           // Integer - Number of visible slides , works only if half option is set to true
        cutFirstAndLast: true,                  // Boolean - if true, cuts the half of first and half of last slide, if circular option is false, and this option is true, current slide is positioned at the beginning, otherwise its at left
        nextBtn: null,                          // Jquery Object ( $('.slider-next-btn') ) - Button for sliding to next slide , if string finds in the container
        prevBtn : null,                         // Jquery Object ( $('.slider-prev-btn') ) - Button for sliding to previous slide, if string finds in the container
        pagination: false,                      // Jquery Object ( $('.pagination') ) - Slide pagination, if string finds in the container
        hideBtnOnTouch: false,                  // show buttons only if no touch events
        paginationText : 'numbers',             // String - dots or numbers, works only if pagination is set
        fadeInSpeed: 400,                       // Integer - Delay of slider fading in after document is ready
        callbackBefore: function () {},         // Function - Params (currSlide, this) - currSlide -> Current element slide (li) - this -> current object
        callbackAfter: function () {}           // Function - Params (currSlide, this) - currSlide -> Current element slide (li) - this -> current object

    };

})(jQuery);
