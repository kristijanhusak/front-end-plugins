/*--------------------------------------
 * Main view
---------------------------------------*/

;(function (App, $) {
    var View = App.Core.View,
        Utils = App.Utils,
        AppView;

    App.Views.App = View.extend({
        initialize: function() {
        	this.render();
        },

        render: function () {
        	// Create instances here
            // App.Instances.Tabs = new App.Views.Tabs();
        }
    });
})(App || {}, jQuery);
