<?php
$depth = 0;

$arr = array(
	1 => array(
		'text' => 'Nom de la',
		'child' => array(
			3 => array(
				'text' => '1',
				'child' => array(
						5 => array(
						'text' => '2',
						'child' => array()
						),
						6 => array(
						'text' => '2',
						'child' => array(
								522 => array(
								'text' => '3',
								'opened' => true,
								'child' => array()
								),
								634 => array(
								'text' => '3',
								'child' => array()
								),
							)
						),
					)
				),
			4 => array(
				'text' => '1',
				'child' => array(
						7 => array(
						'text' => '2',
						'child' => array()
						),
						8 => array(
						'text' => '2',
						'child' => array()
						),
					)
				),
			)
		),
	10 => array(
		'text' => 'Nom de la',
		'child' => array(
			32 => array(
				'text' => '1',
				'child' => array(
						54 => array(
						'text' => '2',
						'child' => array()
						),
						65 => array(
						'text' => '2',
						'child' => array()
						),
					)
				),
			43 => array(
				'text' => '1',
				'child' => array(
						75 => array(
						'text' => '2',
						'child' => array()
						),
						85 => array(
						'text' => '2',
						'child' => array()
						),
					)
				),
			)
		),
	);


function listall($array, $first = true) {

	global $depth;

	$class = ($first) ? 'class="top-level"' : '';
	$html = '<ul '.$class.'>'."\n";

	foreach($array as $k => $v) {
		$btn = (!empty($v['child'])) ? '<a href="#" class="toggle-level"></a>' : '';
		$opened = ($v['opened']) ? ' opened' : '';
		$setclass = (empty($v['child'])) ? 'class="level-'.$depth.$opened.'"' : 'class="'.$opened.'"';

		$html .= '<li data-id="'.$k.'" '.$setclass.'>'.$btn.$v['text'];
		if (!empty($v['child'])) {
			$depth++;
			$html .= listall($v['child'], false);
			$depth--;
		}
		$html .= '</li>'."\n";

	}

	$html .= '</ul>'."\n";
	return $html;
}

function listtable($array) {
	$table = '';
	foreach($array as $k => $v) {
		$table .= '<tr data-id="'.$k.'"><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td><td>'.$v['text'].'</td></tr>';
		if (!empty($v['child'])) {
			$table .= listtable($v['child']);
		}
	}

	return $table;
}

function listactions($array) {
	$action = '';
	foreach($array as $k => $v) {
		$action .= '<tr data-id="'.$k.'"><td><a href="view/'.$k.'">View</a> <a href="edit/'.$k.'">Edit</a> <a href="delete/'.$id.'">Delete</a></td></tr>';
		if (!empty($v['child'])) {
			$action .= listactions($v['child']);
		}
	}

	return $action;
}

function listemptytable($array) {
	$emptytable = '';
	foreach($array as $k => $v) {
		$emptytable .= '<tr data-id="'.$k.'"><td> &nbsp; </td><td></tr>';
		if (!empty($v['child'])) {
			$emptytable .= listemptytable($v['child']);
		}
	}

	return $emptytable;
}