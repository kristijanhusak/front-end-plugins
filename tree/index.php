<?php include('function.php'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="width=device-width">

		<title></title>
		<meta name="description" content="">

		<link rel="stylesheet" href="assets/css/style.css">
		<script src="assets/js/vendor/modernizr-2.6.2.min.js"></script>
	</head>
<body>


<div class="table">
	<section class="left-table">
		<table class="fake-table toggle-rows-table">
			<thead><th>Plus</th><th>Campagne</th></thead>
			<?php echo listemptytable($arr); ?>
		</table>
		<?php echo listall($arr); ?>
	</section>
	<div class="middle-table">
		<table class="toggle-rows-table">
			<thead>
			<tr><th>Levier</th><th>Regie</th><th>Support</th><th>Impressio.</th><th>Clic</th><th>Nb Conv.</th><th>Tx Conv.</th><th>Telecharg</th><th>1 lancem</th><th>CA genere</th><th>Status</th>
			</tr>
			</thead>
			<?php echo listtable($arr); ?>
		</table>
	</div>
	<table class="right-table toggle-rows-table">
		<thead><tr><th>Actions</th></tr></thead>
		<?php echo listactions($arr); ?>
</div>


<!-- javascript -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='assets/js/vendor/jquery-1.9.1.min.js'>\x3C/script>")</script>

<!-- <script src="assets/js/prod/scripts.min.js"></script> -->
<script src="table.js"></script>

<!--[if lt IE 7]><p class="chromeframe">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

</body>
</html>
