(function ( $ ) {
	"use strict";
if ( typeof Object.create !== 'function' ) {
    Object.create = function( obj ) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}

var tableObject = {
	init: function () {
		var self = this;

		self.lvl = $('.toggle-level');
		self.toplvl = $('.top-level');
		self.table = $('.toggle-rows-table');
		self.middleTable = $('.middle-table');

		self.toplvl.find('li').append($('<i />', {'class' : 'vertical' } )).append($('<i />', {'class' : 'horizontal' } ));
		self.vertical = self.toplvl.find('.vertical');
		self.horizontal2 = self.toplvl.find('li.level-2').find('i.horizontal');
		self.horizontal3 = self.toplvl.find('li.level-3').find('i.horizontal');

		self.setOpen();
		self.setData();
		self.setLines();
		self.scrolled = 0;
		

		self.lvl.on('click', function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass('active').siblings('ul').toggleClass('opened');
			self.setLines();
			self.setData();
		});

		self.tableWidth = self.middleTable.width();

		self.middleTable.on('scroll', function () {
			self.setScroll($(this));
		});
	},

	setLines: function () {
		var self = this;
		self.vertical.each(function () {
			var $this = $(this), elem = $this.siblings('ul').children('li').last(),pos;
			if (elem.length) {
				 pos = elem.position().top;
				 if (pos) pos++;
				 $this.outerHeight(pos);
			}
		});
	},

	setData: function () {
		var self = this;
		self.toplvl.find('li').each(function (key, value) {
			var $this = $(this), dataId = $this.attr('data-id');
			if ($this.is(':visible')) {
				self.table.find('tr[data-id=' + dataId + ']').show();
			}
			else {
				self.table.find('tr[data-id=' + dataId + ']').hide();
			}
				
		});
	},

	setOpen: function () {
		var self = this;
		$('.opened').parents('ul').addClass('opened').siblings('.toggle-level').addClass('active');
	},

	setScroll: function (el) {
		var self = this, $this = el, scr = ( $this.scrollLeft() / self.tableWidth) * 10;

			if (scr < 1  && scr < self.scrolled) {
				self.horizontal3.addClass('medium').removeClass('small');
			}
			if (scr <= 0.1) {
				self.horizontal2.removeClass('small');
				self.horizontal3.removeClass('medium small');
			}

			if (scr > 0.3 && scr > self.scrolled) {
				self.horizontal2.addClass('small');
				self.horizontal3.addClass('medium');
			}

			if (scr > 1 && scr > self.scrolled) {
				self.horizontal3.removeClass('medium').addClass('small');
			}


		self.scrolled = scr;
	}

};

	var obj = Object.create(tableObject);
	obj.init();


})( jQuery) ;